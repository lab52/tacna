# Estilo para portal TACNA

## Estructura del gasto enlistado
```
<tr data-url="panel.tpl" data-toggle="slidePanel">
  <td class="cell-60">
    <div class="checkbox-custom checkbox-primary checkbox-lg multi-select">
      <input type="checkbox" class="mailbox-checkbox" id="mail_1" />
      <label for="mail_1"></label>
    </div>
  </td>
  <td class="cell-30 responsive-hide">
    <div class="checkbox-important checkbox-default">
      <input type="checkbox" class="mailbox-checkbox mailbox-important" id="mail_1_important"
      />
    </div>
  </td>
  <td>
    <div class="content">
      <div class="title">El�ctrica del Puerto, S.A. de C.V.</div>
      <div class="abstract">TIJ | 565665 | 03/24/2017</div>
    </div>
  </td>
  <td class="cell-130">
    <div class="time">$414.00 MXN</div>
    <div class="identity"><i class="wb-medium-point green-600" aria-hidden="true"></i>Approved</div>
  </td>
</tr>
```

## Estructura del gasto a detalle (panel)

```
<header class="slidePanel-header">
  <div class="slidePanel-actions" aria-label="actions" role="group">
    <button type="button" class="btn btn-icon btn-pure btn-inverse slidePanel-close actions-top icon wb-close"
    aria-hidden="true"></button>
    <div class="btn-group actions-bottom" role="group">
      <button type="button" class="btn btn-animate btn-animate-side btn-success">
        <span><i class="icon wb-check" aria-hidden="true"></i> Approve</span>
      </button>
      <button data-target="#exampleFormModal" data-toggle="modal"
      type="button" class="btn btn-animate btn-animate-side btn-danger">
        <span><i class="icon wb-close" aria-hidden="true"></i> Reject</span>
      </button>
    </div>
  </div>
  <h1>Nail Alliance Invoice</h1>
  <p>
    <strong>Serie:</strong> TIJ<br>
    <strong>Folio:</strong> 4470<br>
    <strong>Date:</strong> 11/11/2011<br>
  </p>
  <button type="button" class="btn btn-animate btn-animate-side btn-default btn-outline"
  data-target="#pdf" data-toggle="modal" style="background-color: white;">
    <span><i class="icon wb-order" aria-hidden="true"></i> View PDF</span>
  </button>
</header>
<div class="slidePanel-inner">
  <section class="slidePanel-inner-section">      <!-- Panel -->
	  <div class="panel">
	    <div class="panel-body container-fluid">
	      <div class="row">
	        <div class="col-md-5">
	          <h4>BAJA FUR S.A. DE C.V.</h4>
	          <address>
	            123 Avenue #545
	            <br> Tijuana, BC, 22564
	            <br>
	            <abbr title="Mail">E-mail:</abbr>&nbsp;&nbsp;example@google.com
	            <br>
	            <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(123) 456-7890
	            <br>
	            <abbr title="Fax">Fax:</abbr>&nbsp;&nbsp;800-692-7753
	          </address>
	        </div>
	        <div class="col-md-5 col-md-offset-2 text-right">
	          <h4>COMPA�IA EMBOTELLADORA DEL FUERTE, S. DE R.L. DE C.V.</h4>
	          <address>
	            123 Avenue #545
	            <br> Tijuana, BC, 22564
	            <br>
	            <abbr title="Mail">E-mail:</abbr>&nbsp;&nbsp;example@google.com
	            <br>
	            <abbr title="Phone">Phone:</abbr>&nbsp;&nbsp;(123) 456-7890
	            <br>
	            <abbr title="Fax">Fax:</abbr>&nbsp;&nbsp;800-692-7753
	          </address>
	        </div>
	      </div>
	      <div class="page-invoice-table table-responsive">
	        <table class="table table-hover text-right">
	          <thead>
	            <tr>
	              <th class="text-center">#</th>
	              <th>Description</th>
	              <th class="text-right">Quantity</th>
	              <th class="text-right">Unit Cost</th>
	              <th class="text-right">Total</th>
	            </tr>
	          </thead>
	          <tbody>
	            <tr>
	              <td class="text-center">
	                1
	              </td>
	              <td class="text-left">
	                Server hardware purchase
	              </td>
	              <td>
	                32
	              </td>
	              <td>
	                $75
	              </td>
	              <td>
	                $2152
	              </td>
	            </tr>
	            <tr>
	              <td class="text-center">
	                2
	              </td>
	              <td class="text-left">
	                Office furniture purchase
	              </td>
	              <td>
	                15
	              </td>
	              <td>
	                $169
	              </td>
	              <td>
	                $4169
	              </td>
	            </tr>
	            <tr>
	              <td class="text-center">
	                3
	              </td>
	              <td class="text-left">
	                Company Anual Dinner Catering
	              </td>
	              <td>
	                69
	              </td>
	              <td>
	                $49
	              </td>
	              <td>
	                $1260
	              </td>
	            </tr>
	            <tr>
	              <td class="text-center">
	                4
	              </td>
	              <td class="text-left">
	                Payment for Jan 2015
	              </td>
	              <td>
	                149
	              </td>
	              <td>
	                $12
	              </td>
	              <td>
	                $866
	              </td>
	            </tr>
	          </tbody>
	        </table>
	      </div>
	      <div class="text-right clearfix">
	        <div class="pull-right">
	          <p>Sub - Total amount:
	            <span>$4800</span>
	          </p>
	          <p>TAX:
	            <span>$35</span>
	          </p>
	          <p class="page-invoice-amount">Total:
	            <span>$4835</span>
	          </p>
	        </div>
	      </div>
	    </div>
	  </div>
	  <!-- End Panel -->
	</section>
</div>
```